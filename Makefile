# Makefile for python-updater
MAN_INCLUDE=man.include
VERSION=$(shell ./python-updater -V)
FILES=AUTHORS Makefile $(MAN_INCLUDE) python-updater.in python-updater.1
PKGDIR=python-updater-$(VERSION)
TARBALL=$(PKGDIR).tar.bz2
INSTALL ?= install
sbindir = $(EPREFIX)/usr/sbin
mandir = $(EPREFIX)/usr/share/man

all: python-updater python-updater.1

python-updater: python-updater.in
	sed -e "s:@GENTOO_PORTAGE_EPREFIX@:$(EPREFIX):g" $^ > $@
	chmod +x $@

python-updater.1: $(MAN_INCLUDE)
	help2man -L C -Ni $(MAN_INCLUDE) ./python-updater -o $@
	sed -i -e 's/ in the manpage//' \
		-e 's/\*[[:space:]]\([[:alpha:]]*\).*/\1 /' $@

install: python-updater python-updater.1
	$(INSTALL) -d $(DESTDIR)$(sbindir)
	$(INSTALL) -m0755 python-updater $(DESTDIR)$(sbindir)
	$(INSTALL) -d $(DESTDIR)$(mandir)/man1
	$(INSTALL) -m0644 python-updater.1 $(DESTDIR)$(mandir)/man1

.PHONY: all clean tarball upload install
clean: python-updater
	rm -fr python-updater python-updater.1 *.bz2 $(PKGDIR) || true
tarball: $(FILES) python-updater
	mkdir -p $(PKGDIR)
	cp $(FILES) $(PKGDIR)
	tar -cjf $(TARBALL) $(PKGDIR)
	rm -fr $(PKGDIR)
upload:
	scp $(TARBALL) dev.gentoo.org:/space/distfiles-local
	ssh dev.gentoo.org chmod ug+rw /space/distfiles-local/$(TARBALL)
